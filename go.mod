module gitlab.com/purwandi/farm

go 1.12

require (
	github.com/kr/pretty v0.1.0 // indirect
	github.com/satori/go.uuid v1.2.1-0.20181028125025-b2ce2384e17b
	github.com/stretchr/testify v1.3.0
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)

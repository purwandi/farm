package query

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/purwandi/farm/domain"
	"gitlab.com/purwandi/farm/repository"
)

type FarmServiceSuccess struct {
}

func (fs FarmServiceSuccess) IsFarmNameExist(name string) bool {
	return false
}

func TestCanFindByFarmName(t *testing.T) {
	// Given
	fs := FarmServiceSuccess{}

	farm1, _ := domain.CreateFarm(fs, "Sawah Luntu", "Lokasi")
	farm2, _ := domain.CreateFarm(fs, "Sawah Dua", "Lokasi")

	repo := &repository.FarmStorage{
		FarmMap: map[string]domain.Farm{
			farm1.ID: *farm1,
			farm2.ID: *farm2,
		},
	}

	query := NewFarmQuery(repo)

	// When
	farm := query.FindByName(farm1.Name)

	// Then
	assert.Equal(t, farm1.Name, farm.Name)
}

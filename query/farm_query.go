package query

import (
	"gitlab.com/purwandi/farm/domain"
	"gitlab.com/purwandi/farm/repository"
)

// Query is implementation query result
type Query struct {
	Result interface{}
	Error  error
}

// FarmQueryResult is farm query result in memory
type FarmQueryResult struct {
	Storage *repository.FarmStorage
}

// FarmResult is query interface for farm result
type FarmResult interface {
	FindByName(name string) domain.Farm
}

// NewFarmQuery is to create new farm query instance
func NewFarmQuery(storage *repository.FarmStorage) FarmResult {
	return &FarmQueryResult{
		Storage: storage,
	}
}

// FindByName is
func (f *FarmQueryResult) FindByName(name string) domain.Farm {
	farm := domain.Farm{}
	for _, value := range f.Storage.FarmMap {
		if value.Name == name {
			farm = value
			break
		}
	}

	return farm
}

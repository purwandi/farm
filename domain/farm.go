package domain

import (
	"errors"
	"time"
)

// Farm is farm domain
type Farm struct {
	ID       string
	Name     string
	Location string
}

// FarmService is a service to
type FarmService interface {
	IsFarmNameExist(name string) bool
}

// CreateFarm register a farm
func CreateFarm(fs FarmService, name, location string) (*Farm, error) {
	// Validate farm is exist on storage
	ok := fs.IsFarmNameExist(name)
	if ok == true {
		return &Farm{}, errors.New("Farm name is exists")
	}

	return &Farm{
		ID:       time.Now().String(),
		Name:     name,
		Location: location,
	}, nil
}

package domain

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

type FarmServiceSuccess struct {
}

func (fs FarmServiceSuccess) IsFarmNameExist(name string) bool {
	return false
}

type FarmServiceError struct {
}

func (fs FarmServiceError) IsFarmNameExist(name string) bool {
	return true
}

func TestCanCreateFarm(t *testing.T) {
	// Given
	name := "Sawah Luntu"
	location := "Bandung"

	fs := FarmServiceSuccess{}

	// When
	farm, err := CreateFarm(fs, name, location)

	// Then
	assert.Nil(t, err)
	assert.Equal(t, "Sawah Luntu", farm.Name)
}

func TestCanNotCreateFarmIfNameAlreadyExist(t *testing.T) {
	// Given
	name := "Sawah Luntu"
	location := "Bandung"

	fs := FarmServiceError{}

	// When
	_, err := CreateFarm(fs, name, location)

	// Then
	assert.Equal(t, err.Error(), "Farm name is exists")
}

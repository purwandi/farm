package domain

import "time"

// Reservoir is just represent reservoir
type Reservoir struct {
	ID          string
	Name        string
	FarmID      string
	WaterSource WaterSource
}

const (
	// BucketType is a water source type
	BucketType = "BUCKET"
	// TapType is a water source type
	TapType = "TAP"
)

// WaterSource just water source interface
type WaterSource interface {
	Type() string
}

// Bucket is a water source bucket type
type Bucket struct {
	Capacity float32
}

// Type is bucket type implementatio from water source
func (b Bucket) Type() string {
	return BucketType
}

// Tap is a water source tap type
type Tap struct {
}

// Type is tap type implementatio from water source
func (t Tap) Type() string {
	return TapType
}

// CreateBucket registers a new Bucket.
func CreateBucket(capacity float32) Bucket {
	return Bucket{Capacity: capacity}
}

// CreateTap registers a new tap.
func CreateTap() Tap {
	return Tap{}
}

// CreateWaterSource is jus
func CreateWaterSource(name string, capacity float32) WaterSource {
	var ws WaterSource
	if name == BucketType {
		ws = CreateBucket(capacity)
	} else if name == TapType {
		ws = CreateTap()
	}

	return ws
}

// CreateReservoir is to create reservoir
func CreateReservoir(farmID, name, waterSource string, capacity float32) Reservoir {

	ws := CreateWaterSource(waterSource, capacity)

	return Reservoir{
		ID:          time.Now().String(),
		Name:        name,
		WaterSource: ws,
		FarmID:      farmID,
	}
}

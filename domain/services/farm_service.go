package services

import (
	"gitlab.com/purwandi/farm/domain"
	"gitlab.com/purwandi/farm/repository"
)

// FarmServiceInMemory is farm service implementation for
type FarmServiceInMemory struct {
	Repository *repository.FarmStorage
}

// NewFarmServiceInMemory to create instance farm service in memory
func NewFarmServiceInMemory(repo *repository.FarmStorage) domain.FarmService {
	return &FarmServiceInMemory{
		Repository: repo,
	}
}

// IsFarmNameExist check if farm name is exists
func (fs *FarmServiceInMemory) IsFarmNameExist(name string) bool {
	return false
}

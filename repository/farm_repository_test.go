package repository

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/purwandi/farm/domain"
)

type FarmServiceSuccess struct {
}

func (fs FarmServiceSuccess) IsFarmNameExist(name string) bool {
	return false
}


func TestCanSaveFarmIntoStorage(t *testing.T) {
	// Given
	farmService := &FarmServiceSuccess{}
	farm1, _:= domain.CreateFarm(farmService, "Kuncing", "bandung")
	farm2, _:= domain.CreateFarm(farmService, "Kunp", "bandung")

	repo := CreateFarmStorage()

	// When
	repo.Save(farm1)
	repo.Save(farm2)

	// Then
	assert.Equal(t, repo.FarmMap, map[string]domain.Farm{
		farm1.ID: *farm1,
		farm2.ID: *farm2,
	})
	
}

package repository

import (
	"gitlab.com/purwandi/farm/domain"
)

// FarmStorage is implemtation storage
type FarmStorage struct {
	FarmMap map[string]domain.Farm
}

// CreateFarmStorage is to create farm
func CreateFarmStorage() *FarmStorage {
	return &FarmStorage{
		FarmMap: make(map[string]domain.Farm),
	}
}

// Save is attach farm storage
func (fs *FarmStorage) Save(farm *domain.Farm) string {
	fs.FarmMap[farm.ID] = *farm
	return farm.ID
}

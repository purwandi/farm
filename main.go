package main

import (
	"fmt"

	"gitlab.com/purwandi/farm/domain"
	"gitlab.com/purwandi/farm/domain/services"
	"gitlab.com/purwandi/farm/repository"
)

func main() {
	// Initiate storage
	repo := repository.CreateFarmStorage()
	farmService := services.NewFarmServiceInMemory(repo)

	// Process
	farm1, err1 := domain.CreateFarm(farmService, "Sawah 1", "Bandung")

	if err1 != nil {
		fmt.Println(err1.Error())
	}

	farm2, err2 := domain.CreateFarm(farmService, "Sawah 2", "Bandung")
	if err2 != nil {
		fmt.Println(err2.Error())
	}

	// Persist
	repo.Save(farm1)
	repo.Save(farm2)

	fmt.Println(repo)
}

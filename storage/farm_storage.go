package storage

import (
	"gitlab.com/purwandi/farm/domain"
)

// FarmStorage is implemtation storage
type FarmStorage struct {
	FarmMap map[string]domain.Farm
}

// CreateFarmStorage is just create farm storage
func CreateFarmStorage() *FarmStorage {
	return &FarmStorage{
		FarmMap: make(map[string]domain.Farm),
	}
}